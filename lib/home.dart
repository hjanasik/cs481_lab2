import 'package:cs481_lab2/ui/home_screen.dart';
import 'package:cs481_lab2/ui/profil_screen.dart';
import 'package:cs481_lab2/ui/settings_screen.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map<String, Widget> navigationItemBottomBar = const {
    "Home": Icon(Icons.home),
    "Tab 1": Icon(Icons.info),
    "Setting": Icon(Icons.settings)
  };

  @override
  void initState() {
    super.initState();
  }

  Map<int, Widget> navigationBody = {
    0: HomeScreen(),
    1: ProfilScreen(),
    2: SettingScreen(),
  };

  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("App Lab 2")),
      body: navigationBody[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        backgroundColor: Colors.green,
        elevation: 0.0,
        onTap: updateCurrentIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white54,
        items: navigationItemBottomBar.entries.map( (e) =>
          BottomNavigationBarItem(title: Text(e.key), icon: e.value),
        ).toList()),
    );
  }

  void updateCurrentIndex(int index) {
    setState(() { currentIndex = index; });
  }

  /*Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("App Lab 2"),),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Color(0xFF6200EE),
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white.withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        currentIndex: currentIndex,

        onTap: (value) => setState(() { currentIndex = value; }),

        items: [
          BottomNavigationBarItem( title: Text('Favorites'),
            icon: Icon(Icons.favorite), ),
          BottomNavigationBarItem( title: Text('Music'),
            icon: Icon(Icons.music_note), ),
          BottomNavigationBarItem( title: Text('Places'),
            icon: Icon(Icons.location_on), ),
          BottomNavigationBarItem( title: Text('News'),
            icon: Icon(Icons.library_books), ),
        ],

      )      
    );
  }*/

  
}
